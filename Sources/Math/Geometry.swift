//
//  Geometry.swift
//  Math
//
//  Created by Jon Sturk on 2019-04-14.
//

import Foundation

public func == <T> (lhs: Elipse<T>, rhs: Elipse<T>) -> Bool {
	return lhs.eccentricity == rhs.eccentricity && lhs.semiMajorAxis == rhs.semiMajorAxis
}

public struct Elipse<T>: Equatable where T: FloatingPoint {
	public let semiMajorAxis: T
	public let eccentricity: T
	
	public init(semiMajorAxis a: T, eccentricity e: T) {
		self.semiMajorAxis = a
		self.eccentricity = e
	}
	
	public var apoapsis: T {
		return semiMajorAxis * (1 + eccentricity)
	}
	
	public var periapsis: T {
		return semiMajorAxis * (1 - eccentricity)
	}
	
	public var focus: T {
		return semiMajorAxis * eccentricity
	}
	
	public var semiMinorAxis: T {
		return semiMajorAxis * sqrt(1 - eccentricity * eccentricity)
	}
	
	public var perimiter: T {
		let t1: T = (semiMajorAxis - semiMinorAxis)
		let t2: T = (semiMajorAxis + semiMinorAxis)
		let h: T = (t1 * t1) / (t2 * t2)
		
		let t3: T = (10 + sqrt(4 - 3 * h))
		let t4: T = ((3 * h) / t3)
		
		return T.pi * t2 * (1.0 as! T + t4)
	}
}
