//
//  LinAlg.swift
//  Math
//
//  Created by Jon Sturk on 2019-04-14.
//

import simd

public extension double4x4 {
	var translation: double3 {
		return columns.3.xyz
	}
	
	init(translation: double3) {
		self = matrix_identity_double4x4
		columns.3.x = translation.x
		columns.3.y = translation.y
		columns.3.z = translation.z
	}
	
	init(scaling: double3) {
		self = matrix_identity_double4x4
		columns.0.x = scaling.x
		columns.1.y = scaling.y
		columns.2.z = scaling.z
	}
	
	init(scaling: Double) {
		self = matrix_identity_double4x4
		columns.3.w = 1 / scaling
	}
}

public extension float4x4 {
	static var identity: float4x4 {
		return matrix_identity_float4x4
	}
	var translation: float3 {
		return columns.3.xyz
	}
	
	init(translation: float3) {
		self = matrix_identity_float4x4
		columns.3.x = translation.x
		columns.3.y = translation.y
		columns.3.z = translation.z
	}
	
	init(scaling: float3) {
		self = matrix_identity_float4x4
		columns.0.x = scaling.x
		columns.1.y = scaling.y
		columns.2.z = scaling.z
	}
	
	init(scaling: Float) {
		self = matrix_identity_float4x4
		columns.3.w = 1 / scaling
	}
	
	init(rotationX angle: Float) {
		self = matrix_identity_float4x4
		columns.1.y = cos(angle)
		columns.1.z = sin(angle)
		columns.2.y = -sin(angle)
		columns.2.z = cos(angle)
	}
	
	init(rotationY angle: Float) {
		self = matrix_identity_float4x4
		columns.0.x = cos(angle)
		columns.0.z = -sin(angle)
		columns.2.x = sin(angle)
		columns.2.z = cos(angle)
	}
	
	init(rotationZ angle: Float) {
		self = matrix_identity_float4x4
		columns.0.x = cos(angle)
		columns.0.y = sin(angle)
		columns.1.x = -sin(angle)
		columns.1.y = cos(angle)
	}
	
	init(rotation angle: float3) {
		let rotationX = float4x4(rotationX: angle.x)
		let rotationY = float4x4(rotationY: angle.y)
		let rotationZ = float4x4(rotationZ: angle.z)
		self = rotationX * rotationY * rotationZ
	}
	
	//	static func identity() -> float4x4 {
	//		let matrix:float4x4 = matrix_identity_float4x4
	//		return matrix
	//	}
	
	func upperLeft() -> float3x3 {
		let x = columns.0.xyz
		let y = columns.1.xyz
		let z = columns.2.xyz
		return float3x3(columns: (x, y, z))
	}
	
	init(projectionFov fov: Float, near: Float, far: Float, aspect: Float, lhs: Bool = true) {
		let y = 1 / tan(fov * 0.5)
		let x = y / aspect
		let z = lhs ? far / (far - near) : far / (near - far)
		let X = float4( x,  0,  0,  0)
		let Y = float4( 0,  y,  0,  0)
		let Z = lhs ? float4( 0,  0,  z, 1) : float4( 0,  0,  z, -1)
		let W = lhs ? float4( 0,  0,  z * -near,  0) : float4( 0,  0,  z * near,  0)
		self.init()
		columns = (X, Y, Z, W)
	}
	
	// left-handed LookAt
	init(eye: float3, center: float3, up: float3) {
		let z = normalize(eye - center)
		let x = normalize(cross(up, z))
		let y = cross(z, x)
		let w = float3(dot(x, -eye), dot(y, -eye), dot(z, -eye))
		
		let X = float4(x.x, y.x, z.x, 0)
		let Y = float4(x.y, y.y, z.y, 0)
		let Z = float4(x.z, y.z, z.z, 0)
		let W = float4(w.x, w.y, x.z, 1)
		self.init()
		columns = (X, Y, Z, W)
	}
	
	init(orthoLeft left: Float, right: Float, bottom: Float, top: Float, near: Float, far: Float) {
		let X = float4(2 / (right - left), 0, 0, 0)
		let Y = float4(0, 2 / (top - bottom), 0, 0)
		let Z = float4(0, 0, 1 / (far - near), 0)
		let W = float4((left + right) / (left - right),
					   (top + bottom) / (bottom - top),
					   near / (near - far),
					   1)
		self.init()
		columns = (X, Y, Z, W)
	}
}

public extension float3x3 {
	init(normalFrom4x4 matrix: float4x4) {
		self.init()
		columns = matrix.upperLeft().inverse.transpose.columns
	}
}

public extension SIMD3 {
	var xy: SIMD2<Scalar> {
		get {
			return SIMD2<Scalar>(x, y)
		}
		set {
			x = newValue.x
			y = newValue.y
		}
	}
}

public extension SIMD4 {
	init(xyz: SIMD3<Scalar>, w: Scalar) {
		self.init(xyz.x, xyz.y, xyz.z, w)
	}
	init(xy: SIMD2<Scalar>, z: Scalar, w: Scalar) {
		self.init(xy.x, xy.y, z, w)
	}
	var xyz: SIMD3<Scalar> {
		get {
			return SIMD3(x, y, z)
		}
		set {
			x = newValue.x
			y = newValue.y
			z = newValue.z
		}
	}
	
	var xy: SIMD2<Scalar> {
		get {
			return SIMD2<Scalar>(x, y)
		}
		set {
			x = newValue.x
			y = newValue.y
		}
	}
}

public struct JSPlane<T> where T: SIMDScalar {
	public let data: SIMD4<T>
	var label: String?
	
	public init(label lbl: String? = nil, a: T, b: T, c: T, d: T) {
		data = SIMD4<T>(a, b, c, d)
		
		self.label = lbl
	}
	
	var a: T {
		return data.x
	}
	
	var b: T {
		return data.y
	}
	
	var c: T {
		return data.z
	}
	
	var d: T {
		return data.w
	}
	
	public init(data: SIMD4<T>) {
		self.data = data
	}
	
	public var normal: SIMD3<T> {
		return data.xyz
	}
}

public extension JSPlane where T == Float {
	init(v0: SIMD3<T>, v1: SIMD3<T>, v2: SIMD3<T>) {
		let v0_1 = v1 - v0
		let v0_2 = v2 - v0
		
		let norm = cross(v0_1, v0_2)
		self.init(pointOnPlane: v0, planeNormal: norm)
	}
	
	init(pointOnPlane point: SIMD3<T>, planeNormal norm: SIMD3<T>) {
		let tmp = dot(point, norm) * -1
	
		data = SIMD4<T>(norm.x, norm.y, norm.z, tmp)
	}
	
	func normalized() -> JSPlane<T> {
		//let len = sqrt(a * a + b * b + c * c)
		let len = length(data)
		let n = data / SIMD4<T>(len, len, len, len)
		return JSPlane(data: n)
	}

	func vectorProjected(_ vec: SIMD3<T>) -> SIMD3<T> {
		return cross(normal, cross(vec, normal))
	}
	
	func distance(toPoint v: SIMD4<T>) -> T {
		//return p.a * v.x + p.b * v.y + p.c * v.z + p.d
		return dot(data.xyz, v.xyz) + d
	}
	
	func distance(toPoint v: SIMD3<T>) -> T {
		//return p.a * v.x + p.b * v.y + p.c * v.z + p.d
		return dot(data.xyz, v) + d
	}
	
	func isPointInFront(_ point: SIMD3<T>) -> Bool {
		return distance(toPoint: point) > 0
	}
}

public extension JSPlane where T == Double {
	init(v0: SIMD3<T>, v1: SIMD3<T>, v2: SIMD3<T>) {
		let v0_1 = v1 - v0
		let v0_2 = v2 - v0
		
		let norm = cross(v0_1, v0_2)
		self.init(pointOnPlane: v0, planeNormal: norm)
	}
	
	init(pointOnPlane point: SIMD3<T>, planeNormal norm: SIMD3<T>) {
		let tmp = dot(point, norm) * -1
		
		data = SIMD4<T>(norm.x, norm.y, norm.z, tmp)
	}
	
	func normalized() -> JSPlane<T> {
		//let len = sqrt(a * a + b * b + c * c)
		let len = length(data)
		let n = data / SIMD4<T>(len, len, len, len)
		return JSPlane(data: n)
	}
	
	func vectorProjected(_ vec: SIMD3<T>) -> SIMD3<T> {
		return cross(normal, cross(vec, normal))
	}
	
	func distance(toPoint v: SIMD4<T>) -> T {
		//return p.a * v.x + p.b * v.y + p.c * v.z + p.d
		return dot(data.xyz, v.xyz) + d
	}
	
	func distance(toPoint v: SIMD3<T>) -> T {
		//return p.a * v.x + p.b * v.y + p.c * v.z + p.d
		return dot(data.xyz, v) + d
	}
	
	func isPointInFront(_ point: SIMD3<T>) -> Bool {
		return distance(toPoint: point) > 0
	}
}

public extension float4x4 {
	init(_ m: double4x4) {
		self = float4x4(float4(m.columns.0), float4(m.columns.1), float4(m.columns.2), float4(m.columns.3))
	}
}
