struct Math {
    var text = "Hello, World!"
}

public struct Angle<T: FloatingPoint>: Equatable {
	public let radians: T
	public var degrees: T {
		return (radians / T.pi) * 180
	}
	
	public init(radians: T) {
		self.radians = radians
	}
	
	public init(degrees: T) {
		self.radians = (degrees / 180) * T.pi
	}
	
	public static func toRadians(from degrees: T) -> T {
		return (degrees / 180) * T.pi
	}
	
	public static func toDegrees(from radians: T) -> T {
		return (radians / T.pi) * 180
	}
}

public func == <T: FloatingPoint> (lhs: Angle<T>, rhs: Angle<T>) -> Bool {
	return abs(lhs.radians - rhs.radians) < (T.leastNonzeroMagnitude * 10)
}

public struct Distance<T: FloatingPoint>: Equatable {
	public let m: T
	public var km: T {
		return m / 1000
	}
	public var au: T {
		return m / 149597870700
	}
	
	public init(m: T) {
		self.m = m
	}
	
	public init(km: T) {
		self.m = km * 1000
	}
	
	public init(au: T) {
		self.m = au * 149597870700
	}
}

public func == <T: FloatingPoint> (lhs: Distance<T>, rhs: Distance<T>) -> Bool {
	return abs(lhs.m - rhs.m) < (T.leastNonzeroMagnitude * 10)
}
